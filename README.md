# Descripción #
Este repositorio contiene el Trabajo Fin de Grado <<TESAH: Token Economy System for Attention Deficit Hiperactivity Disorder>> realizado por José Duro Gómez.

Este proyecto se basa en el desarrollo de un sistema de gestión de tareas supervisado implementando la técnica de economía de fichas.

# Directorios #

El repositorios se divide en 3 directorios:

* **anteproyecto:** Contiene el anteproyecto de este TFG.
* **memoria:** Contiene los archivos fuente de la memoria en formato .tex (Latex), así como las figuras que incorpora.
* **src:** Contine el código fuente del TFG.

A continuación se muestran los subdirectorios de src:

* **android:** Código fuente de la aplicación móvil. Mantiene un árbol de directorios de Android Studio.
* **web:** Código fuente de la aplicación web. Mantiene el árbol de directorios utilizado en el hosting.