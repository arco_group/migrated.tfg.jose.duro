$(document).ready(function(){
	$("#sendMailButton").click(function() {

		var from = $("#fromLabel").val();
		var to = $("#toLabel").val();
		var subject = $("#subjectLabel").val();
		var message = $("#messageLabel").val();
	
		$.ajax({
			url: "../php/sendMail.php",
			type: "POST",
			data: {from:from, to:to, subject:subject, message:message},

			success: function(html){
				if(html != 'false'){
					$('#success').html("<div class='alert alert-success'>");
					$('#success > .alert-success').html("<button type='button' class='close' data-dismiss='success' aria-hidden='true'>&times;")
							                          .append( "</button>");
					$('#success > .alert-success').append("<strong>Mail sent. </strong>");
				 	$('#success > .alert-success').append('</div>');
					window.location = "../php/student_main.php";
				}else{
					$('#success').html("<div class='alert alert-danger'>");
					$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
																			 .append( "</button>");
					$('#success > .alert-danger').append("<strong>Login failed. </strong>");
			 	  $('#success > .alert-danger').append('</div>');
				}
			},

			beforeSend: function(){
				$('#success').html("Sending...");
			}
		});
	
	});
});

