$(document).ready(function(){
	$("#signinButton").click(function() {

		var usuario = $("#usuario").val();
		var password = $("#password").val();
		$.ajax({
			url: "php/signin.php",
			type: "POST",
			data: {usuario:usuario, password:password},
			success: function(html){

				alert(html);
			
				if(html== 1){
					window.location="php/psicologo.php"; 
				}else if (html=='false\n'){
					$('#success').html("<div class='alert alert-danger'>");
					$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append( "</button>");
					$('#success > .alert-danger').append("<strong>Login failed. </strong>");
			 	    $('#success > .alert-danger').append('</div>');
				} else {
					$('#success').html("Error occurred.");
				}
			},
			beforeSend: function(){
				$('#success').html("Loading...");
			}
		});
		return false;
	});
});
