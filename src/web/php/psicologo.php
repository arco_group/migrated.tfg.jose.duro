<?php>
	require_once "connectdb.php";

	session_start();
	$usuario = $_SESSION['usuario'];
	
	$paciente_query = "SELECT * FROM Paciente WHERE Psicologo_id=(SELECT Usuario_id FROM Psicologo WHERE Usuario='$usuario')";
	$result = mysql_query($paciente_query);
	$num_pacientes = mysql_num_rows($result);
	$_SESSION['num_pacientes'] = $num_pacientes;

	for ($i = 0; $i < $num_pacientes; $i++){		
		$paciente_row = mysql_fetch_assoc($result);
		$u_id=$paciente_row['Usuario_id'];
		$p_query = "SELECT * FROM Usuario WHERE Usuario_id='$u_id'";
		$p_result = mysql_query($p_query);
		$p_row=mysql_fetch_assoc($p_result);
		
		$_SESSION['paciente'.$i] = $p_row['Usuario_id'];
		$_SESSION['paciente'.$i.'nombre'] = $p_row['Nombre'];
		$_SESSION['paciente'.$i.'apellidos'] = $p_row['Apellidos'];		
	}	

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Seguimiento de pacientes del sistem TESAH">
	<meta name="author" content="Jose Duro Gómez">

	<title>Administración</title>

	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="../css/signin.css" rel="stylesheet">
  
</head>

<body style="background: linear-gradient(45deg, #888888 0%, #555555 100%)">
	<div class="row" >		
		<div class="col-md-11">
		<img src="../images/amhida.png" class="img-thumbnail" width=150px/>
		</div>
		
		<div class="col-md-1">
		<a href="#" id="logout" class="btn btn-info">Logout</a>
		</div>
		
   </div>
   
    </br></br>
    
    <form name=pacientes_form method="post" action="#pacientes" >
    
    <div align="center">   
    	<h3 id="paciente"><em><strong>PACIENTES</strong></em></h3>
		<select name="pacientes" onchange="pacientes_form.submit()" style="width:350px;height:40px;font-size:18px;">
			<option selected value="">Seleciona el paciente</option>;
			<?php
				$num_pacientes=$_SESSION['num_pacientes'];
				for ($i = 0; $i < $num_pacientes; $i++){
					echo '<option value="'.$_SESSION['paciente'.$i].'">'.$_SESSION['paciente'.$i]." ".$_SESSION['paciente'.$i.'nombre']." ".$_SESSION['paciente'.$i.'apellidos'].'</option>';
				}
			?>
		</select>
	</div>
	</br>		
		<div class="row">
			<h2>
				<div class="col-md-8">
					<?php 
						$p_id = $_POST['pacientes'];
						$res_nombre=mysql_query("SELECT * FROM Usuario WHERE Usuario_id='$p_id'");
						$row_nombre=mysql_fetch_assoc($res_nombre);
					
						echo "<b>Paciente:</b> ".$row_nombre['Nombre']." ".$row_nombre['Apellidos'].".";
					?>
				</div>
				<div class="col-md-4">
					<?php 
						$p_id = $_POST['pacientes'];
						$res_edad=mysql_query("SELECT * FROM Paciente WHERE Usuario_id='$p_id'");
						$row_edad=mysql_fetch_assoc($res_edad);
						echo "Edad: ".$row_edad['Edad']." años."
					?>
				</div>
			</h2>	
		</div>
		<div class="row">
		</br>
		<h3>
		<div class="col-md-8">
			<?php 
				$p_id = $_POST['pacientes'];
				$res_nombre=mysql_query("SELECT * FROM Usuario WHERE Usuario_id=(SELECT Supervisor_id FROM Paciente WHERE Usuario_id='$p_id')");
				$row_nombre=mysql_fetch_assoc($res_nombre);
				$s_id=$row_nombre['Usuario_id'];
				
				echo "<b>Supervisor:</b> ".$row_nombre['Nombre']." ".$row_nombre['Apellidos'].".";
			?>
			</div>
				
			<div class="col-md-4">
				<?php 
					$p_id = $_POST['pacientes'];
					$res_edad=mysql_query("SELECT * FROM Supervisor WHERE Usuario_id='$s_id'");
					$row_edad=mysql_fetch_assoc($res_edad);
					echo "Edad: ".$row_edad['Edad']." años."
				?>
			</div>
		</h3>	
		</div>
	</div>
	</br> 
	
	<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">
							<strong>TAREAS</strong>
						</h3>
					</div>
			<div class="panel-body">
				<table class="table table-striped table-condensed">
					<thead>
						<tr>									
							<th><strong>Nombre</strong></th>
							<th><strong>Descripción</strong></th>
							<th><strong>Tipo</strong></th>
							<th><strong>Fichas máximas</strong></th>
						</tr>
					</thead> 
					<tbody>
						<?php
							$p_elegido =$_POST["pacientes"]; 
		
							$paciente_query_tareas = "SELECT * FROM Supervisor_Tarea_Recompensa WHERE Supervisor_id=(SELECT Supervisor_id FROM Paciente WHERE Usuario_id='$p_elegido')";
							$paciente_result_tareas = mysql_query($paciente_query_tareas);
							$num_tareas = mysql_num_rows($paciente_result_tareas);
							for ($j = 0; $j < $num_tareas; $j++){
								$tareas_row = mysql_fetch_assoc($paciente_result_tareas);
								$t_id = $tareas_row['Tarea_id'];				
								$t_recompensaID = $tareas_row['Recompensa_id'];
								$t_query = "SELECT * FROM Tarea WHERE Tarea_id=$t_id";
								$t_result = mysql_query($t_query);
								$t_row = mysql_fetch_assoc($t_result);				
								$t_name = $t_row['Nombre'];
								$t_desc = $t_row['Descripcion'];
								$t_tipo = $t_row['Tipo'];				
								$res_fichas=mysql_query("SELECT * FROM Ficha WHERE Recompensa_id='$t_recompensaID'");
								$num_fichas=mysql_num_rows($res_fichas);
										
								echo '<tr>';
								echo'<td>'.$t_row['Nombre'].'</td>';
								echo'<td>'.$t_row['Descripcion'].'</td>';
								echo'<td>'.$t_row['Tipo'].'</td>';
								echo'<td>'.$num_fichas.'</td>';
								echo '</tr>';
			//										echo '<option value="'.$t_id.'">'.$t_name.": ".$t_desc.". Tipo ".$t_tipo.". Máximo de Fichas: ".$num_fichas.'</option>';
							}
											
						?>
					</tbody>
				</table>	
			</div>			
	</div> 
	</br>
	<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">
							<strong>PREMIOS</strong>
						</h3>
					</div>
			<div class="panel-body">
				<table name=table_premios class="table">
					<thead>
						<tr>									
							<th><strong>Nombre</strong></th>
							<th><strong>Descripción</strong></th>
							<th><strong>Fichas para conseguirlo</strong></th>
						</tr>
					</thead> 
					<tbody>
					<?php
						$p_elegido =$_POST["pacientes"];
						$result_premios=mysql_query("SELECT * FROM Premio WHERE Paciente_id='$p_elegido'");
						$num_premios=mysql_num_rows($result_premios);
						for ($i = 0; $i < $num_premios ; $i++){
							$p_row=mysql_fetch_assoc($result_premios);
								echo '<tr>';
								echo'<td>'.$p_row['Nombre'].'</td>';
								echo'<td>'.$p_row['Descripcion'].'</td>';
								echo'<td>'.$p_row['Numero_Fichas'].'</td>';
								echo '</tr>';
						}
			
					?>
					</tbody>
				</table>				
			</div>
	</div>
	</br>
	<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title">
							<strong>TAREAS COMPLETADAS</strong>
						</h3>
					</div>
			<div class="panel-body">
				<table name=table_recompensas class="table">
					<thead>
						<tr>									
							<th><strong>Tarea</strong></th>
							<th><strong>Grado completitud</strong></th>
							<th><strong>Fecha</strong></th>
							<th><strong>Fichas Extra</strong></th>
							<th><strong>Motivo Fichas Extra</strong></th>
							<th><strong>Congelada</strong></th>
							<th><strong>Motivo Congelada</strong></th>
						</tr>
					</thead> 
					<tbody>
					<?php
						$p_elegido =$_POST["pacientes"];
						$result_realizadas=mysql_query("SELECT * FROM Paciente_Tarea_Recompensa WHERE Paciente_id='$p_elegido' ORDER BY Fecha_realizacion");
						$num_realizadas=mysql_num_rows($result_realizadas);
						for ($i = 0; $i < $num_realizadas ; $i++){
							$r_row=mysql_fetch_assoc($result_realizadas);
							$t_id=$r_row['Tarea_id'];
							$t_result=mysql_query("SELECT * FROM Tarea WHERE Tarea_id='$t_id'");				
							$t_row=mysql_fetch_assoc($t_result);
							
							if($r_row['Congelada'] == '0') $f_congelada='No';
							else $f_congelada='Si';
							
								echo '<tr>';
								echo'<td>'.$t_row['Nombre'].'</td>';
								echo'<td>'.$r_row['Grado_completitud'].'</td>';
								echo'<td>'.$r_row['Fecha_realizacion'].'</td>';
								echo'<td>'.$r_row['Puntos_extra'].'</td>';
								echo'<td>'.$r_row['Motivo_extra'].'</td>';
								echo'<td>'.$f_congelada.'</td>';
								echo'<td>'.$r_row['Motivo_congelada'].'</td>';
								echo '</tr>';
						}
			
					?>
					</tbody>
				</table>				
			</div>
		</div>
		
	</form>	
 </br> 
	<div class="mastfoot">
		<div class="inner">
			<p><a> &copy;AMHIDA 2014. </a> </p>
		</div>
	</div>		
	
	<!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    
	<!-- Todos los plugins JavaScript de Bootstrap (también puedes
	incluir archivos JavaScript individuales de los únicos
	plugins que utilices) -->
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/logout.js"></script> 
 
</body>
</html>