package tesah.app;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class DescongelarRecompensa extends Fragment {


    public DescongelarRecompensa() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_descongelar_recompensa, container, false);

        final ArrayList<Recompensa> arrayRecompensasCongeladas = Tools.leerRecompensas(getActivity().getApplicationContext(), "RecompensasCongeladas.obj");
        final ListView listaRecompensas = (ListView) v.findViewById(R.id.listaRecompensasCongeladas);
        final ArrayAdapter<Recompensa> adapter = new ArrayAdapter<Recompensa>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_2,android.R.id.text1,arrayRecompensasCongeladas){
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(arrayRecompensasCongeladas.get(position).getMotivoCongelada());
                int puntos =(int)(arrayRecompensasCongeladas.get(position).getFichasMax() * arrayRecompensasCongeladas.get(position).getGradoCompletitud()) + arrayRecompensasCongeladas.get(position).getFichasExtra();
                text2.setText(puntos+" puntos, realizada el "+arrayRecompensasCongeladas.get(position).getFecha()   );
                return view;
            }
        };
        listaRecompensas.setAdapter(adapter);

        listaRecompensas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(adapterView.getContext());
                builder.setTitle("DESCONGELAR")
                        .setMessage("¿Descongelar esta recompensa?\n")
                        .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                String query = "UPDATE `Paciente_Tarea_Recompensa` SET Congelada=False WHERE PTR_id ='"
                                        + ((Recompensa) adapterView.getAdapter().getItem(i)).getId() + "';";
                                GuardarConsulta.guardar(getActivity().getApplicationContext(), query);
                                GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());
                            }
                        })
                        .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                dialogInterface.cancel();
                            }
                        });

                builder.create().show();
            }
        });

        return v;
    }


}
