package tesah.app;


import java.io.Serializable;
import java.util.Calendar;

public class Tarea implements Serializable{
    String nombre;
    String descripcion;
    String tipo;
    int periodicidad;
    int puntosMaximos;
    String fecha;
    int vecesRealizado;

    public Tarea(String nombre, String descripcion, String tipo, int periodicidad) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.periodicidad = periodicidad;
        this.vecesRealizado = 0;
    }

    public Tarea(Tarea tarea,boolean diaNuevo) {
        this.nombre = tarea.getNombre();
        this.descripcion=tarea.getDescripcion();
        this.tipo=tarea.getTipo();
        this.periodicidad=tarea.getPeriodicidad();
        this.puntosMaximos=tarea.getPuntosMaximos();
        this.fecha=tarea.getFecha();
        if (diaNuevo)
            this.vecesRealizado=0;
        else
            this.vecesRealizado = tarea.getVecesRealizado();

    }

    public Tarea() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPeriodicidad() {
        return periodicidad;
    }

    public void setPeriodicidad(int periodicidad) {
        this.periodicidad = periodicidad;
    }

    public int getPuntosMaximos() {
        return puntosMaximos;
    }

    public void setPuntosMaximos(int puntosMaximos) {
        this.puntosMaximos = puntosMaximos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha() {
        fecha = new java.sql.Date(Calendar.getInstance().getTime().getTime()).toString();
    }

    public int getVecesRealizado() {
        return vecesRealizado;
    }

    public void addVecesRealizado() {
        this.vecesRealizado += 1;
    }
    public boolean completadaDiaria(){
        if(this.vecesRealizado==this.periodicidad)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return nombre;
    }

    public String imprimir() {
        return "Tarea{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", tipo='" + tipo + '\'' +
                ", periodicidad=" + periodicidad +
                ", puntosMaximos=" + puntosMaximos +
                ", fecha=" + fecha +
                ", vecesRealizado=" + vecesRealizado +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(this.nombre.equals(((Tarea)o).getNombre()) && this.descripcion.equals(((Tarea)o).getDescripcion()) && this.periodicidad==((Tarea)o).getPeriodicidad() && this.puntosMaximos==((Tarea)o).getPuntosMaximos())
            return true;
        else
            return false;
    }

}
