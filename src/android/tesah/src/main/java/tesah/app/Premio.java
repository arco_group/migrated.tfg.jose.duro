package tesah.app;

import java.io.Serializable;

public class Premio implements Serializable{
    String nombre;
    String descripcion;
    int n_fichas;

    public Premio(String nombre, String descripcion, int n_fichas) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.n_fichas = n_fichas;
    }

    public Premio(Premio premio) {
        this.nombre = premio.getNombre();
        this.descripcion=premio.getDescripcion();
        this.n_fichas=premio.getN_fichas();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getN_fichas() {
        return n_fichas;
    }

    public void setN_fichas(int n_fichas) {
        this.n_fichas = n_fichas;
    }

    @Override
    public String toString() {
        return nombre +"\t"+n_fichas+"\n"+ descripcion;
    }


    public String imprimir() {
        return "Premio{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", n_fichas=" + n_fichas +
                '}';
    }
}
