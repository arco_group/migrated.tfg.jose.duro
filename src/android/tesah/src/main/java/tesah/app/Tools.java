package tesah.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

public class Tools {
    public static boolean verificaConexion(Context ctx) {

        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    public static ArrayList<Tarea> recuperarTareas(Context ctx,String archivo){
        ArrayList<Tarea> listaTareas = new ArrayList<Tarea>();
        ObjectInputStream in = null;
        Tarea aux = null;
        boolean diaNuevo = true;
        try {
            in = new ObjectInputStream(ctx.openFileInput(archivo));
            /********************************Saco la fecha***************************************/

            if(archivo.equals("TareasDiarias.obj")){
                Log.e("Fecha leida", (in.readObject()).toString());
                diaNuevo=false;
            }
            /***********************************************************************************/

            while(true) {
                aux = new Tarea((Tarea)in.readObject(),diaNuevo);
                listaTareas.add(aux);
                Log.e("Tarea leida "+archivo, aux.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            return listaTareas;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return listaTareas;
    }

    public static void tareaRealizada(Tarea tarea,Context ctx) {
        ObjectOutputStream out = null;
        ArrayList<Tarea> arrayRealizadas;
        ArrayList<Tarea> arrayDiarias;

        try {
            tarea.setFecha();
            tarea.addVecesRealizado();
            Log.e("TAREAS A EXAMINAR", "Veces realizado->"+tarea.getVecesRealizado()+" periodicidad->"+tarea.getPeriodicidad()+" Completado->"+tarea.completadaDiaria());

            arrayRealizadas=recuperarTareas(ctx,"TareasRealizadas.obj");
            arrayRealizadas.add(tarea);

            arrayDiarias=recuperarTareas(ctx,"TareasDiarias.obj");

            /********Se quita la tarea actual de la lista*******/
            arrayDiarias.remove(arrayDiarias.indexOf(tarea));

            /**********Si no esta completada, se vuelve a añadir************/
            if (!tarea.completadaDiaria()) {
                arrayDiarias.add(tarea);
            }

            out = new ObjectOutputStream(ctx.openFileOutput("TareasRealizadas.obj",Context.MODE_PRIVATE));
            for(int i=0;i< arrayRealizadas.size();i++) {
                out.writeObject(arrayRealizadas.get(i));
                Log.e("Tarea Realizada", arrayRealizadas.get(i).toString());
            }
            out.close();

            out = new ObjectOutputStream(ctx.openFileOutput("TareasDiarias.obj",Context.MODE_PRIVATE));

            out.writeObject(new java.sql.Date(new Date().getTime()).toString());
            for(int i=0;i< arrayDiarias.size();i++) {
                out.writeObject(arrayDiarias.get(i));
                Log.e("Tarea Diaria", arrayDiarias.get(i).toString());
            }
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void modificarTareasRealizadas(ArrayList<Tarea> tareas,Context ctx){
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(ctx.openFileOutput("TareasRealizadas.obj",Context.MODE_PRIVATE));
            for(int i=0;i< tareas.size();i++) {
                out.writeObject(tareas.get(i));
                Log.e("TAREA ", tareas.get(i).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Premio> recuperarPremios(Context ctx,String archivo){
        ArrayList<Premio> listaPremios = new ArrayList<Premio>();
        ObjectInputStream in = null;
        Premio aux = null;
        try {
            in = new ObjectInputStream(ctx.openFileInput(archivo));
            while(true) {
                aux = new Premio((Premio)in.readObject());
                listaPremios.add(aux);
                Log.e("PREMIO LEIDA", aux.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
            return listaPremios;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return listaPremios;
    }

    public static void limpiarArchivo(Context ctx, String archivo){
        ObjectOutputStream out = null;
        try {
            out=new ObjectOutputStream(ctx.openFileOutput(archivo,Context.MODE_PRIVATE));
            out.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void actualizarTareas(Context ctx){
        final ArrayList<Tarea>[] tareas = new ArrayList[]{new ArrayList<Tarea>()};
        final int[] continuar = {0};
        SharedPreferences supervisor = ctx.getSharedPreferences("supervisor", Context.MODE_PRIVATE);
        final String supName = supervisor.getString("nombre", "");
        final String supApell = supervisor.getString("apellidos", "");
        ObjectOutputStream out = null;
        if (verificaConexion(ctx)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    tareas[0] = BBDD.getTareas(supName, supApell);
                    continuar[0] =1;
                }
            }).start();
        }

        //esperar la respuesta
        while (continuar[0] ==0) {}
        /*********************/

        try {
            out = new ObjectOutputStream(ctx.openFileOutput("Tareas.obj", Context.MODE_PRIVATE));
            for (int i = 0; i < tareas[0].size(); i++) {
                out.writeObject(tareas[0].get(i));
                Log.e("TAREA", tareas[0].get(i).toString());
            }
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void actualizarPremios(Context ctx){
        final ArrayList<Premio>[] premios = new ArrayList[]{new ArrayList<Premio>()};
        final int[] continuar = {0};
        ObjectOutputStream out = null;

        SharedPreferences usuario = ctx.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String userName = usuario.getString("nombre", "");
        final String userApell = usuario.getString("apellidos", "");
        SharedPreferences supervisor = ctx.getSharedPreferences("supervisor", Context.MODE_PRIVATE);
        final String supName = supervisor.getString("nombre", "");
        final String supApell = supervisor.getString("apellidos", "");


        if (Tools.verificaConexion(ctx)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    premios[0] = BBDD.getPremios(supName, supApell, userName, userApell);
                    continuar[0]=1;
                }
            }).start();
        }

        //esperar la respuesta
        while (continuar[0] ==0) {}
        /*********************/
        try {
            out = new ObjectOutputStream(ctx.openFileOutput("Premios.obj", Context.MODE_PRIVATE));
            for (int i = 0; i < premios[0].size(); i++) {
                out.writeObject(premios[0].get(i));
                Log.e("Premio", premios[0].get(i).toString());
            }
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void actualizarPuntos(final Context ctx,int restantes) {
        final int[][] puntos = {{-1, -1}};

        if (Tools.verificaConexion(ctx)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    puntos[0] = BBDD.getPuntosActuales(ctx);
                }
            }).start();
        }
        while(puntos[0][0]==-1){}
        Log.e("Fichas Restantes",String.valueOf(restantes));
        guardarPuntos(ctx, puntos[0][0], puntos[0][1], restantes);
    }

    public static void guardarPuntos(Context ctx, int puntosActuales, int puntosCongelados,int puntosRestantes) {
        OutputStreamWriter archivo;
        try {
            archivo = new OutputStreamWriter(ctx.openFileOutput("LastState.txt", Context.MODE_PRIVATE));
            archivo.write(puntosActuales+"\n"+puntosCongelados+"\n"+puntosRestantes+"\n");
            archivo.flush();
            archivo.close();
        } catch (IOException e) {
            Log.e("Append line","Error al guardar linea");
            e.printStackTrace();
        }
    }

    public static int getPuntosLocales(Context ctx,int tipo){
        //tipo=> 1->Normal | 2->Congelada | 3->Restantes
        int puntos = 0;
        String texto=null;
        try
        {
            BufferedReader fin = new BufferedReader(new InputStreamReader( ctx.openFileInput("LastState.txt")));
            for(int i =0;i<tipo;i++){
                texto=fin.readLine();
            }
            fin.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna -> "+ ex.toString());
            return 0;
        }
        if(texto==null) texto="0";
        puntos=Integer.parseInt(texto);

        return puntos;

    }

    public static void guardarRecompensas(Context ctx,ArrayList<Recompensa> arrayObjRecompensa,String archivo){
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(ctx.openFileOutput(archivo, Context.MODE_PRIVATE));
            for (int i = 0; i < arrayObjRecompensa.size(); i++) {
                out.writeObject(arrayObjRecompensa.get(i));
            }
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Recompensa> leerRecompensas(Context ctx,String archivo){
        ArrayList<Recompensa> arrayRecompensasCong = new ArrayList<Recompensa>();

        ObjectInputStream in = null;
        Recompensa aux = null;
        try {
            in = new ObjectInputStream(ctx.openFileInput(archivo));
            while(true) {
                aux = new Recompensa((Recompensa)in.readObject());
                arrayRecompensasCong.add(aux);
            }

        } catch (IOException e) {
            e.printStackTrace();
            try {
                in.close();
                Log.e("Cerramos", "Bien");
            } catch (IOException e1) {
                Log.e("No Cerramos","Mal");
                e1.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return arrayRecompensasCong;
    }

    public static void borrarRecompensas(Context ctx,ArrayList<Recompensa> recompensasABorrar){
        String query;
        for (int i=0;i<recompensasABorrar.size();i++){
            query = "DELETE FROM `Paciente_Tarea_Recompensa` WHERE PTR_id="+recompensasABorrar.get(i).getId()+"";
            GuardarConsulta.guardar(ctx,query);
        }
        GuardarConsulta.insertarEnBBDD(ctx);
    }

    public static boolean nuevoDia(Context ctx){
        ObjectInputStream in;
        String texto="";
        String date = new java.sql.Date(new Date().getTime()).toString();
        Log.e("Dia",date);
        try
        {
            in = new ObjectInputStream(ctx.openFileInput("TareasDiarias.obj"));
            texto=in.readObject().toString();
            in.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna -> "+ ex.toString());
            return true;
        }
        Log.e("Texto Fecha",texto);
        if(texto.equals(date))
            return false;
        else
            return true;

    }

    public static void actualizarTareasDiarias(Context ctx){
        if(nuevoDia(ctx)){
            ObjectOutputStream out = null;
            ArrayList<Tarea> tareas = recuperarTareas(ctx,"Tareas.obj");
            try {
                out = new ObjectOutputStream(ctx.openFileOutput("TareasDiarias.obj", Context.MODE_PRIVATE));
                out.writeObject(new java.sql.Date(new Date().getTime()).toString());
                for (int i = 0; i < tareas.size(); i++) {
                    out.writeObject(tareas.get(i));
                    Log.e("TAREA insertada en diarias", tareas.get(i).toString());
                }
                out.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
