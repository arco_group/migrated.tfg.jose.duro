package tesah.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;


public class ConfirmacionTarea extends FragmentActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_confirmacion);

        TextView nombre = (TextView) findViewById(R.id.textNombreTareaConfirmacion);
        TextView fecha = (TextView) findViewById(R.id.textFechaTareaConfirmacion);
        final RadioGroup completitud = (RadioGroup) findViewById(R.id.groupCompletitud);
        final EditText puntosExtra = (EditText) findViewById(R.id.extraPoints);
        final EditText motivo = (EditText) findViewById(R.id.motivoExtra);
        final CheckBox congelado = (CheckBox) findViewById(R.id.checkBoxCongelado);
        final EditText motivoCongelada = (EditText) findViewById(R.id.editTextCongelada);
        Button aceptar = (Button) findViewById(R.id.btAceptarConfirmacion);

        Bundle args =  this.getIntent().getExtras();
        final String nombreTarea = ((Tarea)args.getSerializable("Tarea")).getNombre();
        final String descripTarea = ((Tarea)args.getSerializable("Tarea")).getDescripcion();
        final String fechaTarea = ((Tarea)args.getSerializable("Tarea")).getFecha();



        fecha.setText(fechaTarea.toString());
        nombre.setText(nombreTarea.toString());


        SharedPreferences usuario = getSharedPreferences("user", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editorUser = usuario.edit();
        SharedPreferences supervisor = getSharedPreferences("supervisor",Context.MODE_PRIVATE);
        final String userName = usuario.getString("nombre","");
        final String userApell = usuario.getString("apellidos","");
        final String supName = supervisor.getString("nombre","");
        final String supApell = supervisor.getString("apellidos","");

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int checked = completitud.getCheckedRadioButtonId();
                if (checked != -1){
                    //Date fecha = (Date) Calendar.getInstance().getTime();
                    String gradoCompletitud = "";
                    switch (checked){
                        case R.id.radioButtonMal:
                            gradoCompletitud="Mal";
                            break;
                        case R.id.radioButtonRegular:
                            gradoCompletitud="Regular";
                            break;
                        case R.id.radioButtonBien:
                            gradoCompletitud="Bien";
                            break;
                    }

                    if(puntosExtra.getText().toString().equals("0")){
                        GuardarConsulta.guardar(getApplicationContext(),"INSERT INTO Paciente_Tarea_Recompensa " +
                                "(Fecha_realizacion,Tarea_id,Recompensa_id,Paciente_id,Grado_completitud,Congelada,Motivo_congelada) VALUES" +
                                " ('"+fechaTarea+"',(SELECT Tarea_id FROM Tarea WHERE Nombre='"+nombreTarea+"' AND Descripcion='"+descripTarea+"')," +
                                "(SELECT Recompensa_id FROM Supervisor_Tarea_Recompensa WHERE " +
                                        "Supervisor_id =(SELECT Usuario_id from Usuario WHERE Nombre='"+supName+"' AND Apellidos='"+supApell+"') AND " +
                                        "Tarea_id=(SELECT Tarea_id FROM Tarea WHERE Nombre='"+nombreTarea+"' AND Descripcion='"+descripTarea+"'))," +
                                "(SELECT Usuario_id FROM Usuario WHERE Nombre='"+userName+"' AND Apellidos='"+userApell+"'),'"+gradoCompletitud+"'," +
                                ""+congelado.isChecked()+",'"+motivoCongelada.getText().toString()+"');");
                    }else{
                        GuardarConsulta.guardar(getApplicationContext(),"INSERT INTO Paciente_Tarea_Recompensa " +
                                "(Fecha_realizacion,Tarea_id,Recompensa_id,Paciente_id,Grado_completitud,Congelada,Motivo_congelada,Puntos_extra,Motivo_extra) VALUES" +
                                " ('"+fechaTarea+"',(SELECT Tarea_id FROM Tarea WHERE Nombre='"+nombreTarea+"' AND Descripcion='"+descripTarea+"')," +
                                "(SELECT Recompensa_id FROM Supervisor_Tarea_Recompensa WHERE " +
                                "Supervisor_id =(SELECT Usuario_id from Usuario WHERE Nombre='"+supName+"' AND Apellidos='"+supApell+"') AND " +
                                "Tarea_id=(SELECT Tarea_id FROM Tarea WHERE Nombre='"+nombreTarea+"' AND Descripcion='"+descripTarea+"'))," +
                                "(SELECT Usuario_id FROM Usuario WHERE Nombre='"+userName+"' AND Apellidos='"+userApell+"'),'"+gradoCompletitud+"'," +
                                ""+congelado.isChecked()+",'"+motivoCongelada.getText().toString()+"','"+puntosExtra.getText().toString()+"','"+motivo.getText().toString()+"');");
                    }

                    GuardarConsulta.insertarEnBBDD(getApplicationContext());
                    if(Tools.verificaConexion(getApplicationContext())) {
                        Tools.actualizarPuntos(getApplicationContext(),Tools.getPuntosLocales(getApplicationContext(),3));

                    }

                    editorUser.putBoolean("NuevosPuntos",true).commit();

                    Intent intent = new Intent(getApplicationContext(),SupervisorActivity.class);
                    startActivity(intent);
                    finish();
                }



            }
        });

//        return v;

    }

    @Override
    public void onBackPressed() {
        //No permite volver atras
    }
}
