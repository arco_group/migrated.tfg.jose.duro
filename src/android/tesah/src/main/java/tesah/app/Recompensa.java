package tesah.app;

import java.io.Serializable;

/**
 * Created by joseduro on 30/06/14.
 */
public class Recompensa implements Serializable {
    String id;
    String fecha;
    double gradoCompletitud;
    int fichasMax;
    int fichasExtra;
    String congelada;
    String motivoCongelada;

    public Recompensa(String id,String fecha, double gradoCompletitud, int fichasMax, int fichasExtra, String congelada,String motivoCongelada) {
        this.id = id;
        this.fecha = fecha;
        this.gradoCompletitud = gradoCompletitud;
        this.fichasMax = fichasMax;
        this.fichasExtra = fichasExtra;
        this.congelada = congelada;
        this.motivoCongelada=motivoCongelada;
    }

    public Recompensa(Recompensa recompensa){
        this.id=recompensa.getId();
        this.fecha = recompensa.getFecha();
        this.gradoCompletitud = recompensa.getGradoCompletitud();
        this.fichasMax = recompensa.getFichasMax();
        this.fichasExtra = recompensa.getFichasExtra();
        this.congelada = recompensa.isCongelada();
        this.motivoCongelada=recompensa.getMotivoCongelada();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getGradoCompletitud() {
        return gradoCompletitud;
    }

    public void setGradoCompletitud(double gradoCompletitud) {
        this.gradoCompletitud = gradoCompletitud;
    }

    public int getFichasMax() {
        return fichasMax;
    }

    public void setFichasMax(int fichasMax) {
        this.fichasMax = fichasMax;
    }

    public int getFichasExtra() {
        return fichasExtra;
    }

    public void setFichasExtra(int fichasExtra) {
        this.fichasExtra = fichasExtra;
    }

    public String isCongelada() {
        return congelada;
    }

    public void setCongelada(String congelada) {
        this.congelada = congelada;
    }

    public String getMotivoCongelada() {
        return motivoCongelada;
    }

    public void setMotivoCongelada(String motivoCongelada) {
        this.motivoCongelada = motivoCongelada;
    }
}
