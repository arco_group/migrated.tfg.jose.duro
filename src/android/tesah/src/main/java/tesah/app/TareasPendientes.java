package tesah.app;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


public class TareasPendientes extends Fragment {

    public TareasPendientes() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        String archivo;
        if(container.getId()==R.id.containerUser) archivo="TareasDiarias.obj";
        else archivo ="Tareas.obj";


        final View v = inflater.inflate(R.layout.fragment_tareas_pendientes, container, false);
        final TextView titulo = (TextView) v.findViewById(R.id.textTareasPendientes);
        final ListView listaTareas = (ListView) v.findViewById(R.id.listaTareas);
        final ArrayAdapter<Tarea> adapter = new ArrayAdapter<Tarea>(getActivity(), android.R.layout.simple_list_item_1, Tools.recuperarTareas(getActivity().getApplicationContext(), archivo));
        listaTareas.setAdapter(adapter);
        //Código en cliente
        if(container.getId()==R.id.containerUser){
            titulo.setText("Tareas Pendientes");
            listaTareas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                    AlertDialog.Builder tareaBuilder = new AlertDialog.Builder(adapterView.getContext());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View newLayout = inflater.inflate(R.layout.fragment_tarea_realizada, null);
                    tareaBuilder.setTitle("Tarea");
                    tareaBuilder.setView(newLayout);
                    TextView nombreTarea = (TextView) newLayout.findViewById(R.id.textNombreTarea);
                    TextView descripcionTarea = (TextView) newLayout.findViewById(R.id.textDescripcionTarea);
                    TextView puntosTarea = (TextView) newLayout.findViewById(R.id.textPuntosTarea);
                    nombreTarea.setText(((Tarea) listaTareas.getAdapter().getItem(i)).getNombre());
                    descripcionTarea.setText(((Tarea) listaTareas.getAdapter().getItem(i)).getDescripcion());
                    puntosTarea.setText(String.valueOf(((Tarea) listaTareas.getAdapter().getItem(i)).getPuntosMaximos()));
                    tareaBuilder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Tools.tareaRealizada((Tarea) listaTareas.getAdapter().getItem(i), getActivity().getApplicationContext());
                        }
                    });
                    tareaBuilder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    AlertDialog mostrarTarea = tareaBuilder.create();
                    mostrarTarea.show();
                }
            });

            //Código en supervisor --> Modificar o Borrar Tarea
        }else{
            titulo.setText("Modificar Tareas");
            listaTareas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                    AlertDialog.Builder tareaBuilder = new AlertDialog.Builder(adapterView.getContext());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View newLayout = inflater.inflate(R.layout.fragment_mod_tarea,null);
                    tareaBuilder.setTitle(R.string.titleModTask);
                    tareaBuilder.setView(newLayout);


                    final String[] tipoTarea = {""};


                    final EditText nombre = (EditText) newLayout.findViewById(R.id.nombre);
                    nombre.setText(((Tarea) adapterView.getAdapter().getItem(i)).getNombre());
                    final EditText descripcion = (EditText) newLayout.findViewById(R.id.descripcion);
                    descripcion.setText(((Tarea) adapterView.getAdapter().getItem(i)).getDescripcion());
                    final EditText puntos = (EditText) newLayout.findViewById(R.id.puntos);
                    puntos.setText(String.valueOf(((Tarea) adapterView.getAdapter().getItem(i)).getPuntosMaximos()));
                    final EditText periodicidad = (EditText) newLayout.findViewById(R.id.periodicidad);
                    periodicidad.setText(String.valueOf(((Tarea) adapterView.getAdapter().getItem(i)).getPeriodicidad()));


                    Spinner spinner = (Spinner) newLayout.findViewById(R.id.tipo);
                    final ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                            R.array.tipoTareas,android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                            tipoTarea[0] = adapterView.getItemAtPosition(position).toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });


                    SharedPreferences usuario = getActivity().getSharedPreferences("user",getActivity().MODE_PRIVATE);
                    SharedPreferences supervisor = getActivity().getSharedPreferences("supervisor", getActivity().MODE_PRIVATE);

                    final String supName = supervisor.getString("nombre","");
                    final String supApell = supervisor.getString("apellidos", "");
                    final String userName = usuario.getString("nombre","a");
                    final String userApell = usuario.getString("apellidos","a");


                    tareaBuilder.setPositiveButton(R.string.modificar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int puntosAnteriores = ((Tarea)adapterView.getAdapter().getItem(i)).getPuntosMaximos();
                            int puntosActuales = Integer.parseInt(puntos.getText().toString());

                            if(puntosActuales > puntosAnteriores ){
                                int insertadas = puntosActuales-puntosAnteriores;
                                Log.e("Fichas a insertar",String.valueOf(insertadas));
                                for(int cont =0; cont < insertadas;cont++) {
                                    Log.e("For mas fichas","a");
                                    GuardarConsulta.guardar(getActivity().getApplicationContext(), "INSERT INTO `Ficha` (Recompensa_id) (SELECT `Recompensa_id` FROM Supervisor_Tarea_Recompensa WHERE " +
                                            "Supervisor_id = (SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='" + supName + "' AND Apellidos ='" + supApell + "') AND " +
                                            "Tarea_id = (SELECT Tarea_id from `Tarea` where Nombre='" + nombre.getText().toString() + "' AND Descripcion = '" + descripcion.getText().toString() + "') )");
                                }
                            }else {
                                if (puntosAnteriores > puntosActuales) {
                                    int modFichas = puntosAnteriores-puntosActuales;
                                    Log.e("Menos fichas","a");
                                    GuardarConsulta.guardar(getActivity().getApplicationContext(), "DELETE FROM  `Ficha` WHERE Recompensa_id=(SELECT `Recompensa_id` FROM Supervisor_Tarea_Recompensa WHERE " +
                                            "Supervisor_id = (SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='" + supName + "' AND Apellidos ='" + supApell + "') AND " +
                                            "Tarea_id = (SELECT Tarea_id from `Tarea` where Nombre='" + nombre.getText().toString() + "' AND Descripcion = '" + descripcion.getText().toString() + "') )LIMIT " + modFichas );
                                }
                            }
                            GuardarConsulta.guardar(getActivity().getApplicationContext(),"UPDATE `Tarea` SET " +
                                    "Nombre='"+nombre.getText().toString()+"',Descripcion='"+descripcion.getText().toString()+"',Periodicidad="+periodicidad.getText().toString()+",Tipo='"+tipoTarea[0]+
                                    "' WHERE Nombre='"+((Tarea)adapterView.getAdapter().getItem(i)).getNombre()+"' AND Descripcion='"+((Tarea)adapterView.getAdapter().getItem(i)).getDescripcion()+"';");

                            boolean respuesta=GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());

                            if(Tools.verificaConexion(getActivity().getApplicationContext())) {
                                Log.e("Respuesta Hola",String.valueOf(respuesta));
                                while(respuesta==false){}
                                Log.e("Respuesta Adios",String.valueOf(respuesta));
                                Tools.actualizarTareas(getActivity().getApplicationContext());
                            }
                        }

                    });
                    tareaBuilder.setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int j) {
                            //Revisar Sintaxis
                            GuardarConsulta.borrar(getActivity().getApplicationContext());
                            GuardarConsulta.guardar(getActivity().getApplicationContext(),"DELETE FROM `Ficha` " +
                                    "WHERE Recompensa_id=(SELECT `Recompensa_id` FROM Supervisor_Tarea_Recompensa WHERE Tarea_id=(SELECT `Tarea_id` FROM Tarea WHERE " +
                                    "Nombre='"+((Tarea)adapterView.getAdapter().getItem(i)).getNombre()+
                                    "' AND Descripcion='"+((Tarea)adapterView.getAdapter().getItem(i)).getDescripcion()+"' AND Tipo='"+((Tarea)adapterView.getAdapter().getItem(i)).getTipo()+"'))");

                            GuardarConsulta.guardar(getActivity().getApplicationContext(),"DELETE FROM `Recompensa` " +
                                    "WHERE Recompensa_id=(SELECT `Recompensa_id` FROM Supervisor_Tarea_Recompensa WHERE Tarea_id=(SELECT `Tarea_id` FROM Tarea WHERE " +
                                    "Nombre='"+((Tarea)adapterView.getAdapter().getItem(i)).getNombre()+
                                    "' AND Descripcion='"+((Tarea)adapterView.getAdapter().getItem(i)).getDescripcion()+"' AND Tipo='"+((Tarea)adapterView.getAdapter().getItem(i)).getTipo()+"'))");

                            GuardarConsulta.guardar(getActivity().getApplicationContext(),"DELETE FROM `Supervisor_Tarea_Recompensa` " +
                                    "WHERE Tarea_id=(SELECT `Tarea_id` FROM Tarea WHERE " +
                                    "Nombre='"+((Tarea)adapterView.getAdapter().getItem(i)).getNombre()+
                                    "' AND Descripcion='"+((Tarea)adapterView.getAdapter().getItem(i)).getDescripcion()+"' AND Tipo='"+((Tarea)adapterView.getAdapter().getItem(i)).getTipo()+"')");


                            GuardarConsulta.guardar(getActivity().getApplicationContext(),"DELETE FROM `Tarea` WHERE Nombre='"+((Tarea)adapterView.getAdapter().getItem(i)).getNombre()+
                                    "' AND Descripcion='"+((Tarea)adapterView.getAdapter().getItem(i)).getDescripcion()+"' AND Tipo='"+((Tarea)adapterView.getAdapter().getItem(i)).getTipo()+"';");

                            GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());

                            if(Tools.verificaConexion(getActivity().getApplicationContext())) {
                                Tools.actualizarTareas(getActivity().getApplicationContext());
                            }
                        }
                    });


                    AlertDialog mostrarPremio = tareaBuilder.create();
                    mostrarPremio.show();
                }
            });


        }

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
