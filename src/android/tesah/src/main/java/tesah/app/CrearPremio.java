package tesah.app;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class CrearPremio extends Fragment {

    public CrearPremio(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crear_premio, container, false);

        final EditText nombre = (EditText) v.findViewById(R.id.name);
        final EditText descripcion = (EditText) v.findViewById(R.id.descripcion);
        final EditText puntos = (EditText) v.findViewById(R.id.puntos);
        Button bt = (Button) v.findViewById(R.id.btAceptar);

        SharedPreferences usuario = getActivity().getSharedPreferences("user",getActivity().MODE_PRIVATE);
        SharedPreferences supervisor = getActivity().getSharedPreferences("supervisor", getActivity().MODE_PRIVATE);
        final String supName = supervisor.getString("nombre","");
        final String supApel = supervisor.getString("apellidos", "");
        final String userName = usuario.getString("nombre","a");
        final String userApel = usuario.getString("apellidos","a");

        bt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                GuardarConsulta.guardar(getActivity().getApplicationContext(),"INSERT INTO `Premio` (Nombre,Paciente_id,Supervisor_id,Numero_Fichas,Descripcion) VALUES " +
                        "('"+nombre.getText().toString()+"',(SELECT `Usuario_id` from `Usuario` WHERE Nombre='"+userName+"' AND Apellidos='"+userApel+"')," +
                        "(SELECT `Usuario_id` from `Usuario` WHERE Nombre='"+supName+"' AND Apellidos='"+supApel+"'),"+puntos.getText().toString()+",'"+descripcion.getText().toString()+"');");
                GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());

                if(Tools.verificaConexion(getActivity().getApplicationContext())) {
                    Tools.actualizarPremios(getActivity().getApplicationContext());
                }
//                if(Tools.verificaConexion(getActivity().getApplicationContext())) {
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            BBDD.enviarConsultas(getActivity().getApplicationContext());
//                        }
//                    }).start();
//                }
                nombre.setText("");
                descripcion.setText("");
                puntos.setText("");
            }
        });

        return v;
    }
}