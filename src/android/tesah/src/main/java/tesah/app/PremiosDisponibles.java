package tesah.app;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;


public class PremiosDisponibles extends Fragment {


    public PremiosDisponibles() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_premios_disponibles, container, false);

        ProgressBar progressBarPuntos = (ProgressBar) v.findViewById(R.id.progressBarPuntos);
        ProgressBar progressBarCongelados = (ProgressBar) v.findViewById(R.id.progressBarCongelados);

        TextView puntosActuales = (TextView) v.findViewById(R.id.textPuntosDisponibles);
        TextView puntosCongelados = (TextView) v.findViewById(R.id.textPuntosCongelados);

        int puntos = Tools.getPuntosLocales(getActivity().getApplicationContext(),1) + Tools.getPuntosLocales(getActivity().getApplicationContext(), 3);
        System.out.println("puntosAct-> " + puntos);
        puntosActuales.setText(String.valueOf(puntos));

        int puntoscong = Tools.getPuntosLocales(getActivity().getApplicationContext(), 2);
        System.out.println("puntosCong-> " + puntoscong);
        puntosCongelados.setText(String.valueOf(puntoscong));
        int maxBar;
        if (puntos > puntoscong) maxBar=puntos;
        else maxBar=puntoscong;

        progressBarPuntos.setMax(maxBar);
        progressBarPuntos.setProgress(puntos);
        progressBarPuntos.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);
        progressBarCongelados.setMax(maxBar);
        progressBarCongelados.setProgress(puntoscong);
        progressBarCongelados.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);



        final ArrayList<Premio> premios= Tools.recuperarPremios(getActivity().getApplicationContext(), "Premios.obj");
        final ListView listaPremios = (ListView) v.findViewById(R.id.listaPremios);
        final ArrayAdapter<Premio> adapter = new ArrayAdapter<Premio>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_2,android.R.id.text1,premios){
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                        TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                        text1.setText(premios.get(position).getNombre()+"\t\t\t\t"+premios.get(position).getN_fichas()+" puntos");
                        text2.setText(premios.get(position).getDescripcion());
                        return view;
                    }
                };
        listaPremios.setAdapter(adapter);

        if(R.id.contentSupervisor == container.getId()) {
            TextView titulo = (TextView) v.findViewById(R.id.textPremiosDisponibles);
            titulo.setText("Modificar Premio");
            listaPremios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                    // Toast.makeText(view.getContext(),"Seleccionado "+adapterView.getAdapter().getItem(i).toString(),Toast.LENGTH_LONG).show();
                    AlertDialog.Builder premioBuilder = new AlertDialog.Builder(adapterView.getContext());
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    LinearLayout newLayout = new LinearLayout(view.getContext());
                    premioBuilder.setTitle("Modificar Premio");
                    newLayout.setOrientation(LinearLayout.VERTICAL);
                    TextView textNombre = new TextView(newLayout.getContext());
                    textNombre.setText("Nombre");
                    newLayout.addView(textNombre);
                    final EditText nombre = new EditText(newLayout.getContext());
                    nombre.setText(((Premio) adapterView.getAdapter().getItem(i)).getNombre());
                    newLayout.addView(nombre);
                    TextView textDescrip = new TextView(newLayout.getContext());
                    textDescrip.setText("Descripcion");
                    newLayout.addView(textDescrip);
                    final EditText descripcion = new EditText(newLayout.getContext());
                    descripcion.setText(((Premio) adapterView.getAdapter().getItem(i)).getDescripcion());
                    newLayout.addView(descripcion);
                    TextView textPuntos = new TextView(newLayout.getContext());
                    textPuntos.setText("Puntos");
                    newLayout.addView(textPuntos);
                    final EditText puntos = new EditText(newLayout.getContext());
                    puntos.setText(String.valueOf(((Premio) adapterView.getAdapter().getItem(i)).getN_fichas()));
                    newLayout.addView(puntos);
                    premioBuilder.setView(newLayout);


                    SharedPreferences usuario = getActivity().getSharedPreferences("user", getActivity().MODE_PRIVATE);
                    SharedPreferences supervisor = getActivity().getSharedPreferences("supervisor", getActivity().MODE_PRIVATE);

                    final String supName = supervisor.getString("nombre", "");
                    final String supApel = supervisor.getString("apellidos", "");
                    final String userName = usuario.getString("nombre", "a");
                    final String userApel = usuario.getString("apellidos", "a");


                    premioBuilder.setPositiveButton(R.string.modificar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            GuardarConsulta.guardar(getActivity().getApplicationContext(), "UPDATE `Premio` SET " +
                                    "Nombre='" + nombre.getText().toString() + "',Descripcion='" + descripcion.getText().toString() + "',Numero_Fichas=" + puntos.getText().toString() +
                                    " WHERE Paciente_id=(SELECT `Usuario_id` from `Usuario` WHERE Nombre='" + userName + "' AND Apellidos='" + userApel + "') AND Supervisor_id=(SELECT `Usuario_id` from `Usuario` WHERE Nombre='" + supName + "' AND Apellidos='" + supApel + "')" +
                                    "AND Nombre='" + ((Premio) adapterView.getAdapter().getItem(i)).getNombre() + "';");
                            GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());
                            if (Tools.verificaConexion(getActivity().getApplicationContext())) {
                                Tools.actualizarPremios(getActivity().getApplicationContext());
                            }

                        }

                    });
                    premioBuilder.setNegativeButton(R.string.eliminar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int j) {
                            GuardarConsulta.guardar(getActivity().getApplicationContext(), "DELETE FROM `Premio` WHERE Paciente_id=(SELECT `Usuario_id` from `Usuario` WHERE Nombre='" + userName + "' AND Apellidos='" + userApel + "') AND Supervisor_id=(SELECT `Usuario_id` from `Usuario` WHERE Nombre='" + supName + "' AND Apellidos='" + supApel + "')" +
                                    "AND Nombre='" + ((Premio) adapterView.getAdapter().getItem(i)).getNombre() + "';");
                            GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());
                            if (Tools.verificaConexion(getActivity().getApplicationContext())) {
                                Tools.actualizarPremios(getActivity().getApplicationContext());
                            }
                        }
                    });


                    AlertDialog mostrarPremio = premioBuilder.create();
                    mostrarPremio.show();
                }
            });

        }

        return v;
    }



}
