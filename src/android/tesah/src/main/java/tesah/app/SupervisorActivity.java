package tesah.app;


import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SupervisorActivity extends ActionBarActivity {
    private Menu optionsMenu;

    private DrawerLayout cajon;
    private ActionBarDrawerToggle toggle;
    private android.app.FragmentManager fragmentManager= getFragmentManager();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor);

        SharedPreferences supervisor = getSharedPreferences("supervisor",MODE_PRIVATE);
        String supName = supervisor.getString("nombre","");
        String supApell = supervisor.getString("apellidos","");

        getActionBar().setTitle(supName + " " + supApell);
//        getActionBar().setIcon(android.R.drawable.stat_notify_sync_noanim);


        String[] valores = getResources().getStringArray(R.array.menuSupervisor);
        cajon = (DrawerLayout) findViewById(R.id.drawer_layout);

        TareasPorConfirmar f = new TareasPorConfirmar();
        fragmentManager.beginTransaction().add(R.id.contentSupervisor,f).commit();

        final ListView opciones = (ListView) findViewById(R.id.left_drawer);
        opciones.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, valores));

        toggle = new ActionBarDrawerToggle(this,cajon,android.R.drawable.ic_menu_sort_by_size,R.string.aceptar,R.string.eliminar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        opciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        TareasPorConfirmar a = new TareasPorConfirmar();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor,a).commit();
                        break;
                    case 1:
                        CrearTarea b = new CrearTarea();
//                        fragmentTransaction.replace(R.id.content_frame,b);

                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor,b).commit();
                        break;
                    case 2:
                        TareasPendientes c = new TareasPendientes();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor,c).commit();
                        break;
                    case 3:
                        CrearPremio d = new CrearPremio();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor, d).commit();
                        break;
                    case 4:
                        PremiosDisponibles e = new PremiosDisponibles();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor, e).commit();
                        break;
                    case 5:
                        ConcederPremio f = new ConcederPremio();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor,f).commit();
                        break;
                    case 6:
                        DescongelarRecompensa g = new DescongelarRecompensa();
                        fragmentManager.beginTransaction().replace(R.id.contentSupervisor,g).commit();
                        break;
                }
                cajon.closeDrawer(opciones);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        if (item.getItemId()== R.id.menuRefresh){
            Tools.actualizarTareas(getApplicationContext());
            Tools.actualizarPremios(getApplicationContext());
            Tools.actualizarPuntos(getApplicationContext(),Tools.getPuntosLocales(getApplicationContext(),3));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.refresh,menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.menuRefresh);
            if (refreshItem != null) {
                if (refreshing) {
                    Log.e("Sync","Wola");
                    refreshItem.setIcon(android.R.drawable.stat_notify_sync);
                    Log.e("Sync","Bye");
                } else {
                    Log.e("No Sync","Wola");
                    refreshItem.setIcon(android.R.drawable.stat_notify_sync_noanim);
                    Log.e("No Sync","Bye");
                }
            }
        }
    }

}