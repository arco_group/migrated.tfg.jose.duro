package tesah.app;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class CrearTarea extends Fragment {

    public CrearTarea() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crear_tarea, container, false);

        final String[] tipoTarea = {""};
        final EditText nombre = (EditText) v.findViewById(R.id.nombre);
        final EditText descripcion = (EditText) v.findViewById(R.id.descripcion);
        final EditText puntos = (EditText) v.findViewById(R.id.puntos);
        final EditText periodicidad = (EditText) v.findViewById(R.id.periodicidad);

        Spinner spinner = (Spinner) v.findViewById(R.id.tipo);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.tipoTareas,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                tipoTarea[0] = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button bt = (Button) v.findViewById(R.id.btAceptar);

        SharedPreferences supervisor = getActivity().getSharedPreferences("supervisor", getActivity().MODE_PRIVATE);
        final String supName = supervisor.getString("nombre","");
        final String supApell = supervisor.getString("apellidos", "");

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GuardarConsulta.guardar(getActivity().getApplicationContext(),"INSERT INTO `Tarea` (Nombre,Descripcion,Tipo,Periodicidad) VALUES " +
                        "('"+nombre.getText().toString()+"','"+descripcion.getText().toString()+"','"+tipoTarea[0]+"',"+periodicidad.getText().toString()+");");

                GuardarConsulta.guardar(getActivity().getApplicationContext(),"INSERT INTO `Recompensa` () VALUES ()");

                GuardarConsulta.guardar(getActivity().getApplicationContext(),"INSERT INTO `Supervisor_Tarea_Recompensa` (Tarea_id,Recompensa_id,Fecha_creacion,Supervisor_id) VALUES (" +
                        "(SELECT Tarea_id from `Tarea` where Nombre='"+nombre.getText().toString()+"' AND Descripcion = '"+descripcion.getText().toString()+"')," +
                        "(Select Recompensa_id from `Recompensa` Order by Recompensa_id Desc limit 1)," +
                        "now(), (SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='"+supName+"' AND Apellidos ='"+supApell+"'));");

                int aux = Integer.parseInt(puntos.getText().toString());

                for(int i =0; i<aux;i++)
                    GuardarConsulta.guardar(getActivity().getApplicationContext(),"INSERT INTO `Ficha` (Recompensa_id) (SELECT `Recompensa_id` FROM Supervisor_Tarea_Recompensa WHERE " +
                            "Supervisor_id = (SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='"+supName+"' AND Apellidos ='"+supApell+"') AND " +
                            "Tarea_id = (SELECT Tarea_id from `Tarea` where Nombre='"+nombre.getText().toString()+"' AND Descripcion = '"+descripcion.getText().toString()+"') )");
                GuardarConsulta.insertarEnBBDD(getActivity().getApplicationContext());

                if(Tools.verificaConexion(getActivity().getApplicationContext())) {
                    Tools.actualizarTareas(getActivity().getApplicationContext());
                }
//                if(Tools.verificaConexion(getActivity().getApplicationContext())) {
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            BBDD.enviarConsultas(getActivity().getApplicationContext());
//                        }
//                    }).start();
//                }
                nombre.setText("");
                descripcion.setText("");
                puntos.setText("");

            }
        });

        return v;
    }
}