package tesah.app;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class GuardarConsulta {

    public static void guardar(Context ctx, String query) {
        OutputStreamWriter archivo;
        try {
            archivo = new OutputStreamWriter(ctx.openFileOutput("Query.txt", Context.MODE_APPEND));
            archivo.write(query + "\n");
            archivo.flush();
            Log.e("ARCHIVO", "Escrito " + query);
            archivo.close();
        } catch (IOException e) {
            Log.e("Append line","Tururu al guardar linea");
            e.printStackTrace();
        }
    }
    public static void borrar(Context ctx){
        OutputStreamWriter archivo;
        try{
            archivo = new OutputStreamWriter(ctx.openFileOutput("Query.txt",Context.MODE_PRIVATE));
            archivo.write("");
            archivo.close();
        } catch (IOException e) {
            Log.e("Append line","Tururu al guardar linea");
            e.printStackTrace();
        }
    }

    public static boolean insertarEnBBDD(final Context ctx){
        final boolean[] respuesta = new boolean[1];
        respuesta[0]=false;
        if(Tools.verificaConexion(ctx)){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    respuesta[0] =BBDD.enviarConsultas(ctx);
                }
            }).start();
            while (respuesta[0]==false){}
            return respuesta[0];
        }
        return respuesta[0];
    }

}