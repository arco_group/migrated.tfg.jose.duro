package tesah.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joseduro on 5/06/14.
 */
public class BBDD{
    static String result = "";
    static InputStream is;

    public static Boolean enviarConsultas(Context ctx){
        try
        {
            BufferedReader fin = new BufferedReader(new InputStreamReader( ctx.openFileInput("Query.txt")));
            String texto;
            Log.e("Consulta","Entra");
            while((texto=fin.readLine()) != null) {
                Log.e("Fichero", texto);
                ejecutarInsert(texto);
            }
            fin.close();
            GuardarConsulta.borrar(ctx);
            return true;
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna -> "+ ex.toString());
            ex.printStackTrace();
            return false;
        }
    }

    /*POR HACER https*/

    public static String ejecutarInsert(String query){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("query",query));

//http post
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.amhida.hol.es/tesah/php/connectdb.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){
            Log.e("log_tag", "Error in http connection\n");
            e.printStackTrace();
        }
        //convert response to string
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();

            result=sb.toString();

            return result;
        }catch(Exception e){
            Log.e("log_tag", "Error converting result "+e.toString());
        }

       return result;
    }

    public static ArrayList<Tarea> getTareas (String supName, String supApell){
        ArrayList<Tarea> tareas=new ArrayList<Tarea>();
        String res;
        String query = ("SELECT * FROM Supervisor_Tarea_Recompensa WHERE Supervisor_id=(SELECT Usuario_id FROM Usuario WHERE Nombre='"+supName+"' AND Apellidos='"+supApell+"');");
        res=ejecutarInsert(query);

        //parse json data
        try{
            JSONArray jArray = new JSONArray(res);
            Log.e("JSONArray",jArray.toString());
            Tarea tarea;
            for(int i=0;i<jArray.length();i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                String resTareas = ejecutarInsert("SELECT * FROM Tarea WHERE Tarea_id="+jObject.getString("Tarea_id")+";");
                JSONArray arrayTarea = new JSONArray(resTareas);
                JSONObject jObjectTarea = arrayTarea.getJSONObject(0);
                tarea = new Tarea(jObjectTarea.getString("Nombre"),jObjectTarea.getString("Descripcion"),jObjectTarea.getString("Tipo"),Integer.parseInt(jObjectTarea.getString("Periodicidad")));
                String resPuntosTarea = ejecutarInsert("SELECT * FROM Ficha WHERE Recompensa_id=(SELECT Recompensa_id FROM Supervisor_Tarea_Recompensa WHERE " +
                        "Tarea_id="+jObject.getString("Tarea_id")+" AND Supervisor_id=(SELECT Usuario_id FROM Usuario WHERE Nombre='"+supName+"' AND Apellidos='"+supApell+"'))");
                System.out.println("Array Puntos Tarea "+resPuntosTarea);
                JSONArray arrayPuntosTarea = new JSONArray(resPuntosTarea);
                tarea.setPuntosMaximos(arrayPuntosTarea.length());
                Log.e("Tarea Completa",tarea.imprimir());
                tareas.add(tarea);
            }

        } catch (JSONException e1) {
            Log.e("log_tag", "Error parsing data "+e1.toString());
            e1.printStackTrace();
        }

        return tareas;

    }

    public static ArrayList<Premio> getPremios (String supName, String supApell,String userName, String userApell){
        ArrayList<Premio> premios=new ArrayList<Premio>();
        String res;
        String query = ("SELECT * FROM Premio WHERE Supervisor_id=(SELECT Usuario_id FROM Usuario WHERE Nombre='"+supName+"' AND Apellidos='"+supApell+"') " +
                "AND Paciente_id=(SELECT Usuario_id FROM Usuario WHERE Nombre='"+userName+"' AND Apellidos='"+userApell+"');");
        Log.e("Select premio",query);
        res=ejecutarInsert(query);

        //parse json data
        try{
            JSONArray jArray = new JSONArray(res);
            Log.e("JSONArray",jArray.toString());
            Premio premio;
            for(int i=0;i<jArray.length();i++) {
                JSONObject jObjectPremio = jArray.getJSONObject(i);
                premio = new Premio(jObjectPremio.getString("Nombre"),jObjectPremio.getString("Descripcion"),Integer.parseInt(jObjectPremio.getString("Numero_Fichas")));

                Log.e("Premio",premio.imprimir());
                premios.add(premio);
            }

        } catch (JSONException e1) {
            Log.e("log_tag", "Error parsing data "+e1.toString());
            e1.printStackTrace();
        }

        return premios;

    }

    public static int[] getPuntosActuales(Context ctx){
        int puntos[] = {0,0};
        Recompensa objRecompensa;
        ArrayList<Recompensa> arrayObjRecompensa = new ArrayList<Recompensa>();
        ArrayList<Recompensa> arrayObjRecompensaCong = new ArrayList<Recompensa>();
        SharedPreferences usuario = ctx.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String userName = usuario.getString("nombre", "");
        final String userApell = usuario.getString("apellidos", "");

        final String query = "Select * FROM Paciente_Tarea_Recompensa WHERE Paciente_id=(SELECT Usuario_id FROM Usuario WHERE Nombre='"+userName+"' AND Apellidos='"+userApell+"')";
        String rowRecompensas = ejecutarInsert(query);
        Map<Integer,Integer> recompensas = new HashMap<Integer, Integer>();
        try{

            JSONArray jArray = new JSONArray(rowRecompensas);
            Log.e("JSON ARRAY filas P_T_R",jArray.toString());
            for(int i=0;i<jArray.length();i++){
                double grado=0;
                int extra=0;
                JSONObject jObject = jArray.getJSONObject(i);

                if(jObject.getString("Grado_completitud").equals("Bien")){
                    grado=1;
                }else{
                    if(jObject.getString("Grado_completitud").equals("Regular"))
                        grado=0.5;
                }

                if(!jObject.getString("Puntos_extra").equals("null"))
                    extra=jObject.getInt("Puntos_extra");

                int recompensa = Integer.parseInt(jObject.getString("Recompensa_id"));

                if(!recompensas.containsKey(recompensa)) {
                    recompensas.put(recompensa,nFichasRecompensa(jObject.getString("Recompensa_id")));
                }
                objRecompensa = new Recompensa(jObject.getString("PTR_id"),jObject.getString("Fecha_realizacion"),grado,recompensas.get(recompensa),extra,jObject.getString("Congelada"),jObject.getString("Motivo_congelada"));
                //Log.e("Fichas",String.valueOf(recompensas.get(recompensa).intValue()));
                if(jObject.getString("Congelada").equals("0")) {
                    puntos[0] += (recompensas.get(recompensa).intValue() * grado) + extra;
                    arrayObjRecompensa.add(objRecompensa);
                }
                else {
                    puntos[1] += (recompensas.get(recompensa).intValue() * grado) + extra;
                    arrayObjRecompensaCong.add(objRecompensa);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Tools.guardarRecompensas(ctx, arrayObjRecompensa,"Recompensas.obj");
        Tools.guardarRecompensas(ctx,arrayObjRecompensaCong,"RecompensasCongeladas.obj");
        return puntos;
    }

    public static int nFichasRecompensa(String recompensa_id){
        final String query = "SELECT * FROM Ficha WHERE Recompensa_id='"+recompensa_id+"';";

        JSONArray jArray = null;
        try {
            jArray = new JSONArray(ejecutarInsert(query));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jArray.length();

    }



}
