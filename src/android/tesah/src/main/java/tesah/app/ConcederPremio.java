package tesah.app;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class ConcederPremio extends Fragment {


    public ConcederPremio() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_premios_disponibles, container, false);

        final TextView puntosActuales = (TextView) v.findViewById(R.id.textPuntosDisponibles);
        TextView puntosCongelados = (TextView) v.findViewById(R.id.textPuntosCongelados);

        int puntos = Tools.getPuntosLocales(getActivity().getApplicationContext(), 1) + Tools.getPuntosLocales(getActivity().getApplicationContext(), 3);
        System.out.println("puntosAct-> " + puntos);
        puntosActuales.setText(String.valueOf(puntos));

        puntos = Tools.getPuntosLocales(getActivity().getApplicationContext(), 2);
        System.out.println("puntosCong-> " + puntos);
        puntosCongelados.setText(String.valueOf(puntos));

        final ArrayList<Premio> premios= Tools.recuperarPremios(getActivity().getApplicationContext(), "Premios.obj");
        final ListView listaPremios = (ListView) v.findViewById(R.id.listaPremios);
        final ArrayAdapter<Premio> adapter = new ArrayAdapter<Premio>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_2,android.R.id.text1,premios){
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setText(premios.get(position).getNombre()+"\t\t\t\t"+premios.get(position).getN_fichas()+" puntos");
                text2.setText(premios.get(position).getDescripcion());
                return view;
            }
        };
        listaPremios.setAdapter(adapter);

        listaPremios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
                if(((Premio)adapterView.getAdapter().getItem(i)).getN_fichas()< Integer.parseInt(puntosActuales.getText().toString())){
                    AlertDialog.Builder builder = new AlertDialog.Builder(adapterView.getContext());
                    builder.setTitle("CONCEDER PREMIO")
                            .setMessage("¿Conceder Premio "+((Premio) adapterView.getAdapter().getItem(i)).getNombre()+"?")
                            .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int j) {
                                    ArrayList<Recompensa> arrayRecompensas = Tools.leerRecompensas(getActivity().getApplicationContext(),"Recompensas.obj");
                                    ArrayList<Recompensa> arrayRecompensasABorrar = new ArrayList<Recompensa>();
                                    int puntos = Tools.getPuntosLocales(getActivity().getApplicationContext(),3);
                                    Log.e("Puntos antes de sumar",String.valueOf(puntos));
                                    int objetivo = ((Premio) adapterView.getAdapter().getItem(i)).getN_fichas();
                                    int k,fichasAGuardar;
                                    for(k=0;(k<arrayRecompensas.size()) && (puntos < objetivo);k++){
                                        int aux=(int)(arrayRecompensas.get(k).getFichasMax()*arrayRecompensas.get(k).getGradoCompletitud()) + arrayRecompensas.get(k).getFichasExtra();
                                        puntos+= aux;
                                        arrayRecompensasABorrar.add(arrayRecompensas.get(k));
                                    }
                                    if (puntos > objetivo){
                                        fichasAGuardar=puntos-objetivo;
                                    }else{
                                        fichasAGuardar=0;
                                    }

                                    Log.e("Puntos despues de sumar",String.valueOf(fichasAGuardar));

                                    Tools.actualizarPuntos(getActivity().getApplicationContext(),fichasAGuardar);

                                    Tools.borrarRecompensas(getActivity().getApplicationContext(),arrayRecompensasABorrar);


                                }
                            })
                            .setNegativeButton(R.string.cancelar,new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Log.e("Puntos Actuales",String.valueOf(Tools.getPuntosLocales(getActivity().getApplicationContext(),1)));
                                    Log.e("Puntos congelados",String.valueOf(Tools.getPuntosLocales(getActivity().getApplicationContext(),2)));
                                    Log.e("Puntos restantes",String.valueOf(Tools.getPuntosLocales(getActivity().getApplicationContext(),3)));

                                    dialogInterface.cancel();
                                }
                            });
                    builder.create().show();


                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(adapterView.getContext());
                    builder.setTitle("PUNTOS INSUFICIENTES")
                            .setMessage("Puntos disponibles insuficientes")
                            .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    builder.create().show();
                }
            }
        });

        return v;
    }


}
