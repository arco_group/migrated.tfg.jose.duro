package tesah.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

import tesah.app.util.SystemUiHider;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class LoginActivity extends Activity {
    int option=-1;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().hide();

        //First execution
        if(recoverPassword().equals("DEFAULT")){
            firstExecution();
            cargarPantallaLogin();
        }else {
            LoadViewTask task = new LoadViewTask();
            task.execute();
        }
/**************************************************************************/
        /*final ListView usersList = (ListView) findViewById(R.id.userList);
//        String[] roles = {"Usuario","Supervisor"};
        final ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.users,android.R.layout.simple_list_item_single_choice);
        usersList.setAdapter(adapter);
        usersList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        usersList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                option=arg2;
                ListView lv = (ListView) arg0;
                TextView tv = (TextView) lv.getChildAt(arg2);
                String s = tv.getText().toString();
            } });

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch(option){
                    case 0:
                        intent=new Intent(LoginActivity.this, UserActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        AlertDialog dialog = showPasswordDialog();
                        dialog.show();
                        break;
                    default:
                        break;
                }
            }
        });*/

    }



    public void firstExecution(){
        //Crear Archivo temporal Querys
        OutputStreamWriter archivo=null;
        ObjectOutputStream out=null;
        try
        {
            archivo= new OutputStreamWriter(openFileOutput("Query.txt", Context.MODE_APPEND));
            archivo.close();
            archivo= new OutputStreamWriter(openFileOutput("LastState.txt", Context.MODE_PRIVATE));
            archivo.close();
            out=new ObjectOutputStream(openFileOutput("Tareas.obj",Context.MODE_PRIVATE));
            out.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero en memoria interna");
            ex.printStackTrace();
        }
        //Inicializar tesah
        initSupervisor();
    }

    public AlertDialog showPasswordDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View layout = inflater.inflate(R.layout.password,null);
        builder.setTitle(R.string.password);
        builder.setView(layout)
                // Add action buttons
                .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        EditText checkPass = (EditText) layout.findViewById(R.id.getPass);
                        if(checkPassword(checkPass.getText().toString())){
                            Intent intent = new Intent(LoginActivity.this, SupervisorActivity.class);
                            startActivity(intent);
                        }else{
                            String message = "La contraseña es incorrecta." + " \n" + "Inténtalo de nuevo.";
                            AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                            builder.setTitle("Error");
                            builder.setMessage(message);
                            builder.setNeutralButton("Aceptar", null);
                            builder.create().show();
                        }

                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });


        return builder.create();

    }

    public void initSupervisor(){
        AlertDialog.Builder iniSupBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View newLayout = inflater.inflate(R.layout.fragment_inicializar_user,null);
        iniSupBuilder.setTitle(R.string.titleInitSup);
        iniSupBuilder.setView(newLayout);
        iniSupBuilder.setNeutralButton(R.string.aceptar, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                EditText name = (EditText) newLayout.findViewById(R.id.name);
                EditText apellidos = (EditText) newLayout.findViewById(R.id.apellidos);
                EditText edad = (EditText) newLayout.findViewById(R.id.edad);

                SharedPreferences supervisor = getSharedPreferences("supervisor",MODE_PRIVATE);
                SharedPreferences.Editor editor = supervisor.edit();
                editor.putString("nombre",name.getText().toString());
                editor.putString("apellidos",apellidos.getText().toString());
                editor.putString("edad",edad.getText().toString());
                editor.commit();
                Toast.makeText(LoginActivity.this, "Supervisor "+name.getText().toString() +" Guardado", Toast.LENGTH_LONG).show();


                GuardarConsulta.guardar(getApplicationContext(), "INSERT INTO `Usuario` (`Nombre`, `Apellidos`, `Tipo`) " +
                        "VALUES ('" + name.getText().toString() + "', '" + apellidos.getText().toString() + "', 'Supervisor');");
                GuardarConsulta.insertarEnBBDD(getApplicationContext());
                //Crear Password Administrador
                changePassword();

            }
        });
        AlertDialog iniSup =  iniSupBuilder.create();
        iniSup.show();
    }

    public void initUsuario(){
        AlertDialog.Builder iniUserBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View newLayout = inflater.inflate(R.layout.fragment_inicializar_user,null);
        iniUserBuilder.setTitle(R.string.titleIniUser);
        iniUserBuilder.setView(newLayout);
        iniUserBuilder.setNeutralButton(R.string.aceptar, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                EditText name = (EditText) newLayout.findViewById(R.id.name);
                EditText apellidos = (EditText) newLayout.findViewById(R.id.apellidos);
                EditText edad = (EditText) newLayout.findViewById(R.id.edad);


                SharedPreferences usuario = getSharedPreferences("user",MODE_PRIVATE);
                SharedPreferences.Editor editorUser = usuario.edit();

                editorUser.putString("nombre",name.getText().toString());
                editorUser.putString("apellidos",apellidos.getText().toString());
                editorUser.putString("edad", edad.getText().toString());
                editorUser.commit();
                Toast.makeText(LoginActivity.this, "Usuario "+name.getText().toString() +" Creado", Toast.LENGTH_LONG).show();

                SharedPreferences supervisor = getSharedPreferences("supervisor",MODE_PRIVATE);
                String supName = supervisor.getString("nombre","");
                String supApell = supervisor.getString("apellidos","");

                GuardarConsulta.guardar(getApplicationContext(), "INSERT INTO `Usuario` (`Nombre`, `Apellidos`, `Tipo`) " +
                        "VALUES ('" + name.getText().toString() + "', '" + apellidos.getText().toString() + "', 'Usuario');");

                GuardarConsulta.guardar(getApplication(),"INSERT INTO `Paciente` (Usuario_id,Edad,Ultima_Actualizacion,Psicologo_id,Supervisor_id)"+
                        "VALUES( (SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='"+name.getText().toString()+"' AND Apellidos ='"+apellidos.getText().toString()+"'),"+edad.getText().toString()+",NOW(),1,(SELECT `Usuario_id` FROM `Usuario` WHERE Nombre ='"+supName+"' AND Apellidos ='"+supApell+"'));");
                GuardarConsulta.insertarEnBBDD(getApplicationContext());

                elegirIcono();

            }
        });
        AlertDialog iniUser =  iniUserBuilder.create();
        iniUser.show();
    }

    private void elegirIcono() {
        SharedPreferences usuario = getSharedPreferences("user",MODE_PRIVATE);
        final SharedPreferences.Editor editor = usuario.edit();
        final AlertDialog.Builder iniIconBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View newLayout = inflater.inflate(R.layout.elegir_icono,null);
        iniIconBuilder.setTitle("Elige tu icono");
        iniIconBuilder.setView(newLayout);

        ImageButton robot = (ImageButton) newLayout.findViewById(R.id.imageRobot);
        ImageButton chica = (ImageButton) newLayout.findViewById(R.id.imageChica);

        robot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Icono","Elige robot");
                editor.putInt("icono",R.drawable.robot).commit();
            }
        });

        chica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("icono",R.drawable.chica).commit();
            }
        });

        iniIconBuilder.setNeutralButton(R.string.aceptar,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog iniIcon =  iniIconBuilder.create();
        iniIcon.show();
    }

    public void changePassword(){
        AlertDialog.Builder newPassBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View newLayout = inflater.inflate(R.layout.newpassword,null);
        newPassBuilder.setTitle("Nueva Contraseña");
        newPassBuilder.setView(newLayout);
        newPassBuilder.setNeutralButton(R.string.aceptar, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                EditText pass1 = (EditText) newLayout.findViewById(R.id.getNewPass);
                EditText pass2 = (EditText) newLayout.findViewById(R.id.getNewPass2);

                if(pass1.getText().toString().equals(pass2.getText().toString())){
                    storePassword(pass1.getText().toString());
                    Toast.makeText(LoginActivity.this, "Contraseña guardada", Toast.LENGTH_LONG).show();


                    SharedPreferences supervisor = getSharedPreferences("supervisor",MODE_PRIVATE);
                    String nombre = supervisor.getString("nombre","");
                    String apellidos = supervisor.getString("apellidos","");
                    String edad = supervisor.getString("edad","");

                    GuardarConsulta.guardar(getApplicationContext(), "INSERT INTO `Supervisor` " +
                            "SELECT `Usuario_id`,"+ edad +",'"+pass1.getText().toString()+"' FROM `Usuario` WHERE Nombre ='"+nombre+"' AND Apellidos ='"+apellidos+"';");
                    GuardarConsulta.insertarEnBBDD(getApplicationContext());

                    //Inicializar Usuario
                    initUsuario();

                }else{
                    Toast.makeText(LoginActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_LONG).show();
                    changePassword();
                }
            }
        });
        AlertDialog newPass =  newPassBuilder.create();
        newPass.show();
    }

    public boolean checkPassword(String checkPass){
        String storePass = recoverPassword();
        if(checkPass.equals(storePass) ){
            return true;
        }
        return false;
    }

    //Metods to store and recover the password

    public void storePassword(String password){
        SharedPreferences settings = getSharedPreferences("supervisor", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("password",password );
        editor.commit();
    }

    public String recoverPassword(){
        SharedPreferences settings = getSharedPreferences("supervisor", MODE_PRIVATE);
        String pref= settings.getString("password", "DEFAULT");
        return pref;
    }



    //AsyncTask para la carga de la aplicación.
    private class LoadViewTask extends AsyncTask<Void, Integer, Void>
    {
        //Before running code in separate thread
        @Override
        protected void onPreExecute()
        {
            //Create a new progress dialog
            progressDialog = new ProgressDialog(LoginActivity.this);
            //Set the progress dialog to display a horizontal progress bar
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            //Titulo
            progressDialog.setTitle("Cargando...");
            //Cuerpo
            progressDialog.setMessage("Descargando datos, por favor espere...");
            //Opciones varias
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.show();
        }

        //The code to be executed in a background thread.
        @Override
        protected Void doInBackground(Void... params)
        {
            //Ejecución en segundo plano
            if(Tools.verificaConexion(getApplicationContext())) {
                Tools.actualizarTareas(getApplicationContext());
                Tools.actualizarPremios(getApplicationContext());
                Tools.actualizarPuntos(getApplicationContext(),Tools.getPuntosLocales(getApplicationContext(),3));

            }
            Tools.actualizarTareasDiarias(getApplicationContext());


            try
            {
                //Get the current thread's token
                synchronized (this)
                {
                    //Initialize an integer (that will act as a counter) to zero
                    int counter = 0;
                    //While the counter is smaller than four
                    while(counter <= 4)
                    {
                        //Wait 850 milliseconds
                        this.wait(850);
                        //Increment the counter
                        counter++;
                        //Set the current progress.
                        //This value is going to be passed to the onProgressUpdate() method.
                        publishProgress(counter*25);
                    }
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        //Update the progress
        @Override
        protected void onProgressUpdate(Integer... values)
        {
            //set the current progress of the progress dialog
            progressDialog.setProgress(values[0]);
        }

        //after executing the code in the thread
        @Override
        protected void onPostExecute(Void result)
        {
            //close the progress dialog
            progressDialog.dismiss();
            //initialize the View
            cargarPantallaLogin();
        }
    }

    public void cargarPantallaLogin(){
        setContentView(R.layout.activity_login);

        final ListView usersList = (ListView) findViewById(R.id.userList);
//        String[] roles = {"Usuario","Supervisor"};
        final ArrayAdapter adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.users,android.R.layout.simple_list_item_single_choice);
        usersList.setAdapter(adapter);
        usersList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        usersList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                option=arg2;
                ListView lv = (ListView) arg0;
                TextView tv = (TextView) lv.getChildAt(arg2);
                String s = tv.getText().toString();
            } });

        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch(option){
                    case 0:
                        intent=new Intent(LoginActivity.this, UserActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        AlertDialog dialog = showPasswordDialog();
                        dialog.show();
                        break;
                    default:
                        break;
                }
            }
        });
    }
}

