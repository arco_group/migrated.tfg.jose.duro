package tesah.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;


public class UserActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        SharedPreferences usuario = getSharedPreferences("user",MODE_PRIVATE);
        SharedPreferences.Editor editor = usuario.edit();
        String userName = usuario.getString("nombre","");
        String userApell = usuario.getString("apellidos","");

        getActionBar().setTitle(userName+" "+userApell);

        if (usuario.getBoolean("NuevosPuntos",false)){
            MediaPlayer mp = MediaPlayer.create(this, R.raw.ta_da);
            mp.start();
            editor.putBoolean("NuevosPuntos",false).commit();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("¡¡¡ENHORABUENA!!!")
                    .setIcon(usuario.getInt("icono", R.drawable.ic_launcher))
                    .setMessage("Tienes nuevos puntos!!!")
                    .setNeutralButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
            builder.create().show();
        }


        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.Tab tab1 = actionBar.newTab()
                .setText("Premios")
                .setTabListener(new ActionBar.TabListener() {
                    Fragment fragment = Fragment.instantiate(getApplicationContext(),PremiosDisponibles.class.getName());
                    @Override
                    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                        fragmentTransaction.replace(R.id.containerUser,fragment);
                    }

                    @Override
                    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                        fragmentTransaction.remove(fragment);
                    }

                    @Override
                    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

                    }
                });
        actionBar.addTab(tab1);

        ActionBar.Tab tab2 = actionBar.newTab()
                .setText("Tareas")
                .setTabListener(new ActionBar.TabListener() {
                    Fragment fragment = Fragment.instantiate(getApplicationContext(),TareasPendientes.class.getName());
                    @Override
                    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                        fragmentTransaction.replace(R.id.containerUser,fragment);
                    }

                    @Override
                    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                        fragmentTransaction.remove(fragment);
                    }

                    @Override
                    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

                    }
                });
        actionBar.addTab(tab2);

    }

}
