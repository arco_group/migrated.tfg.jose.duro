package tesah.app;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class TareasPorConfirmar extends Fragment {


    public TareasPorConfirmar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tareas_por_confirmar, container, false);

        final ListView listaTareas = (ListView) v.findViewById(R.id.listaTareas);
        final ArrayAdapter<Tarea> adapter = new ArrayAdapter<Tarea>(getActivity(),android.R.layout.simple_list_item_1,Tools.recuperarTareas(getActivity().getApplicationContext(),"TareasRealizadas.obj"));
        listaTareas.setAdapter(adapter);

        listaTareas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity().getApplicationContext(),ConfirmacionTarea.class);
                intent.putExtra("Tarea",adapter.getItem(i));
                Log.e("Tarea a meter", adapter.getItem(i).imprimir());
                ArrayList<Tarea> arrayAux = Tools.recuperarTareas(getActivity().getApplicationContext(), "TareasRealizadas.obj");
                arrayAux.remove(i);
                Tools.modificarTareasRealizadas(arrayAux,getActivity().getApplicationContext());
                startActivity(intent);
                getActivity().finish();
            }
        });

        return v;
    }


}
